package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {

    public static void main(String[] args) {
        /*
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();
    }


    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public void run() {
        // TODO: Implement Rock Paper Scissors

        while (true) {
            System.out.println(String.format("Let's play round %d", roundCounter));
            String human_choice = user_choice();

            String computer_choice = random_choice();
            String choice_string = String.format("Human chose %s, computer chose %s.",human_choice,computer_choice);

            if(is_winner(human_choice, computer_choice)){
                System.out.println(choice_string + " Human wins!");
                humanScore+=1;
            } else if(is_winner(computer_choice, human_choice)){
                System.out.println(choice_string + " Computer wins!");
                computerScore+=1;
            } else{
                System.out.println(choice_string + " It's a tie!");
            }
            System.out.println(String.format("Score: human %d, computer %d",computerScore,humanScore));

            String continue_answer = continue_playing();
            if(continue_answer.equals("n")) break;
        }

        System.out.println("Bye bye :)");
    }

    public String random_choice() {
        return rpsChoices.get((int) (Math.random() * rpsChoices.size()));
    }

    public Boolean is_winner(String choice1, String choice2) {
        if (choice1.equals("paper")) {
            return choice2.equals("rock");
        } else if (choice1.equals("scissors")) {
            return choice2.equals("paper");
        } else {
            return choice2.equals("scissors");
        }
    }

    
    public String user_choice(){
        while(true){
            String human_choice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            if(validate_input(human_choice, rpsChoices)){
                return human_choice;
            } else{
                System.out.println(String.format("I don't understand %s. Could you try again?", human_choice));
            }
        }
    }
    public String continue_playing() {

        while (true) {
            String continue_answer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            if (validate_input(continue_answer, Arrays.asList("y", "n"))) {
                return continue_answer;

            } else {
                System.out.println(String.format("I don't understand %s. Could you try again?", continue_answer));
            }
        }

    }

    public Boolean validate_input(String input, List<String> valid_input) {
        input = input.toLowerCase();
        return valid_input.contains(input);
    }
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
